/* DIY Float Temp PWM board 7th Order Ai Speak
 *   (AKA The Ryan)
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *
 */

const int PWM1Pin = 3;
const int PWM2Pin = 5;
const int PWM3Pin = 6;
const int PWM4Pin = 9;
const int PWM5Pin = 10;
const int PWM6Pin = 11;

const int Temp1Pin = 20;
const int Temp2Pin = 21;
const int Temp3Pin = 14;
const int Temp4Pin = 19;
const int Temp5Pin = 18;
const int Temp6Pin = 17;
const int Temp7Pin = 16;
const int Temp8Pin = 15;

const int Float1Pin = 4;
const int Float2Pin = 2;
const int Float3Pin = 7;
const int Float4Pin = 8;
const int Float5Pin = 13;
const int Float6Pin = 12;


int PWM1 = 0;
int PWM2 = 0;
int PWM3 = 0;
int PWM4 = 0;
int PWM5 = 0;
int PWM6 = 0;

int Temp1 = 0;
int Temp2 = 0;
int Temp3 = 0;
int Temp4 = 0;
int Temp5 = 0;
int Temp6 = 0;
int Temp7 = 0;
int Temp8 = 0;

int Float1 = 0;
int Float2 = 0;
int Float3 = 0;
int Float4 = 0;
int Float5 = 0;
int Float6 = 0;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int actuatorVal = 0;
int Comand = 0;
int Comand1 = 0;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String CheckSum = "00000003";   // <********** SET GROUP ID HERE *************
String CheckSumIn = "";
boolean Pause = false;

void setup() {
  Serial.begin(57600);
    // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  }
  
void loop() {
    
TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
  /*    Serial.print(CheckSum);  //Level Sensor Group ID
      Serial.print("/");
      Serial.print("efl00001:");  //Flow
      Serial.print(CalFlow);
      Serial.print(",");
      Serial.print("etf00001:");  //Temp in Farenheight
      Serial.print(CalTempf);
      Serial.print(",");
      Serial.print("etc00001:");  //Temp in Celcius
      Serial.print(CalTempc);
      Serial.print(",");
      Serial.print("elw00001:");  //Water level in 0-100%
      Serial.print(CalLevel);  //STOP SENDING HERE for 6th ORDER
      Serial.print(",");
      Serial.print("eal00001:");    //Ambient Light Level
      Serial.print(CalAmbient);
      Serial.print(",");
      Serial.print("evo00001:");    //Voltage of Board
      Serial.println(CalVoltage);*/
      LastSend = millis() - 250;  //The program takes about 250ms to run
    }
  }


      //Decides what to do when it recieves a serial command
  if (stringComplete) {
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
 
      switch (Comand) {
      case 0:
        //read device ID from flash memory
         Serial.print("GroupID");
        Serial.println(CheckSum);
        // clear the string:
        inputString = "";
        stringComplete = false;
        Comand = 0;
        Comand1 = 0;
        break;

      case 1:
        //Send Back Sensor Data
        Serial.print(CheckSum);  //Ryan's Hub
        Serial.println("No Sensors Attached");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 2:
        //Set PWM #1 pin
        PWM1 = calcfunction(inputString);
        PWM1 = PWM1 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM1 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM1Pin, LOW);
          Serial.println("1");  
        }
        else {
          analogWrite(PWM1Pin, PWM1);
          Serial.println("1");  
        }
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 3:
        //Set PWM #2 pin
        PWM2 = calcfunction(inputString);
        PWM2 = PWM2 * 255 / 100;  //convert to 0-255 for analog write
        if (PWM2 == 0) {    //PWM pin #3&5 don't turn off all the way, this fixes that
          digitalWrite(PWM2Pin, LOW);
          Serial.println("1");  
        }
        else {
          analogWrite(PWM2Pin, PWM2);
        Serial.println("1");
        }
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 4:
        //Set PWM #3 pin  
        PWM3 = calcfunction(inputString);
        PWM3 = PWM3 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM3Pin, PWM3);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 5:
        //Set PWM #4 pin    
        PWM4 = calcfunction(inputString);
        PWM4 = PWM4 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM4Pin, PWM4);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;

      case 6:
        //Set PWM #5
        PWM5 = calcfunction(inputString);
        PWM5 = PWM5 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM5Pin, PWM5);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;

      case 7:
        //Set PWM #6
        PWM6 = calcfunction(inputString);
        PWM6 = PWM6 * 255 / 100;  //convert to 0-255 for analog write
        analogWrite(PWM6Pin, PWM6);
        Serial.println("1");
        // clear the string:
        inputString = "";
        stringComplete = false; 
        break;
        
      case 10: 
         //Send Back Sensor Data to Ai
         
         // clear the string:
        inputString = "";
        stringComplete = false;
        break;

      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;

      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
      } 
    } 
    }
    
    
    
    
    
    
      //Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the Checksum
        char CheckSumRead = inputString.charAt(i);
        CheckSumIn += CheckSumRead;
      }
      if (CheckSumIn == CheckSum) {   //If the CheckSums (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        CheckSumIn = "";
      } 
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(CheckSum);
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 9 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}
