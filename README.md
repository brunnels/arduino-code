# README #
 All code is Copyright: GNU General Public License V3
 All Schematis and PCB Files are Licensed: Creative Commons Attribution-ShareAlike 3.0 Unported License


### Arduino Code for the Reef Ai Periferals ###
* This is the code that is running on each of the sensors and periferals of the Reef Ai
* All the Arduino powered sensors comunicate with a Python program running on the 'Brain'
* If you are interested in participating, contact: Staff@ReefAi.com
* You'll need to put the Libraries in the 'Libraries' folder in your 'Sketchbook'
      -These are available as a Zip in the Downloads section.


### Serial Comunication Explanation: ###
*   Baud Rate 57600, 8 data bits, no parity, one stop bit.
*    Send all messages with the group ID first
*    Send a "0!" to get the group ID  (ID for the arduino)
*    An invalid group ID returns "-1"
*    Send a "<GroupID>10!" to recieve the sensor data.
*    Send a "<GroupID>11:<DelayInMiliSeconds>!" for a 1 minute stream of data with the delay specified.
*          Example: 0000000211:1000!     Will send the sensor data about once a second for 1 minute.
*    If you ask for something else while it is doing it's 1 minute data stream, it pauses the stream, responds to the request, then resumes streaming.
*
*
###  Element ID's:
*     'e' is for sensor, 'a' for actuator
*     'tf'  <-Temp Farenheight
*     'tc' <-Temp Celcius
*     'fl'  <-flow
*     'lw' <-Water Level
*     'pr' <-PAR
*     'ph' <-pH
*     'sl' <-Salinity
*     'or' <-ORP
*     'al' <-Ambient Light
*     'vo' <-Voltage of incoming DC electricity or battery
*     'wt' <-If Sundrop is wet or dry
*  
### Contact Info:
*  Email: Staff@ReefAi.com
*  Website: www.ReefAi.com