/* Level Sensor 7th Order Ai Speak
 *
 * Copyright 2015 Scott Tomko, Greg Tomko, Linda Close
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * (GNU General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact:  Staff@ReefAi.com
 *

Arduino Pinout: 
2 = capsense fill 1
3 = capsense read 1
4 = capsense read 2
5 = capsense read 6
6 = capsense read 7
7 = capsense read 8
8 = capsense read 9
9 = capsense read 10
10 = capsense read 5
11 = capsense read 4
12 = capsense read 11
13 = capsense read 3
14 = Flow senser power
15 = Ambient Light Sensor
16 = Temp read
17 = Flow read
21 = 3.3v reference
*/

#include <CapacitiveSensor.h>
#include <CapacitiveSensor.h>    //Capacitive Sensing Library
#include <Wire.h>       //I2C Library
#include "TLC59108.h"   //LED Controller Library

#define HW_RESET_PIN 2
#define I2C_ADDR TLC59108::I2C_ADDR::BASE
TLC59108 leds(I2C_ADDR);


String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int Comand = 0;
int Comand1 = 0;
int actuatorVal = 0;
int LED = 0;
byte LEDHEXbyte;
unsigned long TimeNow = 0;
unsigned long StartTime = 0;
unsigned long LastSend = 0;
boolean SendData = false;
boolean SendQue = false;
int SendDelay = 0;
String CheckSum = "00000001";  // <********** SET GROUP ID HERE *************
String CheckSumIn = "";
boolean Pause = false;

int Temp = 0;
int TempAvg = 0;
int CalTempc = 0;
int CalTempf = 0;
int Flow = 0;
int FlowAvg = 0;
int CalFlow = 0;
int Ambient = 0;
int AmbientAvg = 0;
int CalAmbient = 0;
int vRef3p3 = 0;
int vRef3p3Avg = 0;
int CalVoltage = 0;
int CalLevel = 0;


const int FlowPowerPin = 14;  //turns on pwr to flow heater
const int TempPin = 16;
const int FlowReadPin = 17;
const int AmbientPin = 15;
const int vRef3p3Pin =21;

const int capsensefillPin1 = 2;
const int capsensereadPin1 = 3;
const int capsensereadPin2 = 4;
const int capsensereadPin6 = 5;
const int capsensereadPin7 = 6;
const int capsensereadPin8 = 7;
const int capsensereadPin9 = 8;
const int capsensereadPin10 = 9;
const int capsensereadPin5 = 10;
const int capsensereadPin4 = 11;
const int capsensereadPin11 = 12;
const int capsensereadPin3 = 13;

//capacitive sensor
long capVal11 = 0;
long capVal10 = 0;
long capVal9 = 0;
long capVal8 = 0;
long capVal7 = 0;
long capVal6 = 0;
long capVal5 = 0;
long capVal4 = 0;
long capVal3 = 0;
long capVal2 = 0;
long capVal1 = 0;
 //Definng which pins are the (Fill,Sense) pins.
CapacitiveSensor   levelSensor11 = CapacitiveSensor(2,12);
CapacitiveSensor   levelSensor10 = CapacitiveSensor(2,9);
CapacitiveSensor   levelSensor9 = CapacitiveSensor(2,8);
CapacitiveSensor   levelSensor8 = CapacitiveSensor(2,7);
CapacitiveSensor   levelSensor7 = CapacitiveSensor(2,6);
CapacitiveSensor   levelSensor6 = CapacitiveSensor(2,5);
CapacitiveSensor   levelSensor5 = CapacitiveSensor(2,10);
CapacitiveSensor   levelSensor4 = CapacitiveSensor(2,11);
CapacitiveSensor   levelSensor3 = CapacitiveSensor(2,13);
CapacitiveSensor   levelSensor2 = CapacitiveSensor(2,4);
CapacitiveSensor   levelSensor1 = CapacitiveSensor(2,3);



void setup() {
  Serial.begin(57600);
 // reserve 200 bytes for the inputString:
  inputString.reserve(200);
 
   // turn off callibration
  levelSensor11.set_CS_Timeout_Millis(4000);
  levelSensor11.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor10.set_CS_Timeout_Millis(4000);
  levelSensor10.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor9.set_CS_Timeout_Millis(4000);
  levelSensor9.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor8.set_CS_Timeout_Millis(4000);
  levelSensor8.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor7.set_CS_Timeout_Millis(4000);
  levelSensor7.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor6.set_CS_Timeout_Millis(4000);
  levelSensor6.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor5.set_CS_Timeout_Millis(4000);
  levelSensor5.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor4.set_CS_Timeout_Millis(4000);
  levelSensor4.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor3.set_CS_Timeout_Millis(4000);
  levelSensor3.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor2.set_CS_Timeout_Millis(4000);
  levelSensor2.set_CS_AutocaL_Millis(0xFFFFFFFF);
  levelSensor1.set_CS_Timeout_Millis(4000);
  levelSensor1.set_CS_AutocaL_Millis(0xFFFFFFFF);

  Wire.begin();
  leds.init(HW_RESET_PIN);
  leds.setLedOutputMode(TLC59108::LED_MODE::PWM_IND);

  pinMode(FlowPowerPin, OUTPUT);
  pinMode(AmbientPin, INPUT);
  pinMode(vRef3p3Pin, INPUT);
  
} 

// the loop routine runs over and over again forever:
void loop() {
  
  TimeNow = millis();  //Check the time
  if ((StartTime + 60000 > TimeNow) && (SendQue)) { //do this for 1 minute 
    if (Pause) {
      SendData = false;
      Pause = false;
    }
    else {
      SendData = true;
    }
  }
  else {  //  Stop Sending Data
    SendData = false;
    SendQue = false;
    StartTime = 0;
    SendDelay = 0;
    LastSend = 0;
  }
  
  if (SendData) {  //Send Data at the specified Rate
    if (TimeNow > LastSend + SendDelay) {
          //Send Back Sensor Data to Ai
      Serial.print(CheckSum);  //Level Sensor Group ID
      Serial.print("/");
      Serial.print("efl00001:");  //Flow
      Serial.print(CalFlow);
      Serial.print(",");
      Serial.print("etf00001:");  //Temp in Farenheight
      Serial.print(CalTempf);
      Serial.print(",");
      Serial.print("etc00001:");  //Temp in Celcius
      Serial.print(CalTempc);
      Serial.print(",");
      Serial.print("elw00001:");  //Water level in 0-100%
      Serial.print(CalLevel);  //STOP SENDING HERE for 6th ORDER
      Serial.print(",");
      Serial.print("eal00001:");    //Ambient Light Level
      Serial.print(CalAmbient);
      Serial.print(",");
      Serial.print("evo00001:");    //Voltage of Board
      Serial.println(CalVoltage);
      LastSend = millis() - 250;  //The program takes about 250ms to run
    }
  }


      //Decides what to do when it recieves a serial command
  if (stringComplete) {
    Comand = inputString.charAt(8) - '0';  //converting char to int
    Comand1 = inputString.charAt(9) - '0';  //converting char 2 to int
    if ((Comand1 >= 0) && (Comand1 <= 9) && (Comand1 != ':')) {
      Comand = 10;
      Comand += inputString.charAt(9) - '0';
    }
    
    switch (Comand) {
      case 0:
        //read device ID from flash memory
        Serial.print("GroupID");
        Serial.println(CheckSum);
        // clear the string:
        inputString = "";
        stringComplete = false;
        Comand = 0;
        Comand1 = 0;
        break;
      
      case 1:
         //Send Back Sensor Data
        Serial.print(CheckSum);  //Level Sensor
        Serial.print("/");  
        Serial.print("Flow,");
        Serial.print(Flow);
        Serial.print(",");
        Serial.print("Temp,");
        Serial.print(CalTempf);
        Serial.print(",");
        Serial.print("Ambient,");
        Serial.print(Ambient);
        Serial.print(",");
        Serial.print("3.3vRef,");
        Serial.print(vRef3p3);
        Serial.print(",");
        Serial.print("CapSense11,");
        Serial.print(capVal11);
        Serial.print(",");
        Serial.print("CapSense10,");
        Serial.print(capVal10);
        Serial.print(",");
        Serial.print("CapSense9,");
        Serial.print(capVal9);
        Serial.print(",");
        Serial.print("CapSense8,");
        Serial.print(capVal8);
        Serial.print(",");
        Serial.print("CapSense7,");
        Serial.print(capVal7);
        Serial.print(",");
        Serial.print("CapSense6,");
        Serial.print(capVal6);
        Serial.print(",");
        Serial.print("CapSense5,");
        Serial.print(capVal5);
        Serial.print(",");
        Serial.print("CapSense4,");
        Serial.print(capVal4);
        Serial.print(",");
        Serial.print("CapSense3,");
        Serial.print(capVal3);
        Serial.print(",");
        Serial.print("CapSense2,");
        Serial.print(capVal2);
        Serial.print(",");
        Serial.print("CapSense1,");
        Serial.println(capVal1);
        // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 2:
          //Turn ON flow meter power
        digitalWrite(FlowPowerPin, HIGH);
        Serial.println("Flow Heater ON");
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 3:
          //Clear the string
        digitalWrite(FlowPowerPin, LOW);
        Serial.println("Flow Heater OFF");
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 4:
          //Turn on LEDs

        LED = calcfunction(inputString);  //Grab the dimmed value
        actuatorVal = actuatorVal * 255 / 100;  //convert to 0-255 for analog write
        LEDHEXbyte = (byte)LED;      //Convert to a byte for the LED driver
        leds.setAllBrightness(LEDHEXbyte);  //Set all the LED's brightness
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
        
      case 10: 
         //Send Back Sensor Data to Ai
        Serial.print(CheckSum);  //Level Sensor Group ID
        Serial.print("/");
        Serial.print("efl00001:");  //Flow
        Serial.print(CalFlow);
        Serial.print(",");
        Serial.print("etf00001:");  //Temp in Farenheight
        Serial.print(CalTempf);
        Serial.print(",");
        Serial.print("etc00001:");  //Temp in Celcius
        Serial.print(CalTempc);
        Serial.print(",");
        Serial.print("elw00001:");  //Water level in 0-100%
        Serial.print(CalLevel);  //STOP SENDING HERE for 6th ORDER
        Serial.print(",");
        Serial.print("eal00001:");    //Ambient Light Level
        Serial.print(CalAmbient);
        Serial.print(",");
        Serial.print("evo00001:");    //Voltage of Board
        Serial.println(CalVoltage);
          // clear the string:
        inputString = "";
        stringComplete = false;
        break;
         
      case 11:
          //Send Back Data for 60 Seconds
        SendDelay = calcfunction(inputString);  //Grab the time in milliseconds
        StartTime = millis();                   //to delay before sending again
        SendData = true;
        SendQue = true;
          // clear the string:
        inputString = "";
        stringComplete = false;
        Serial.println("1");
        break;
      
      default:
        Serial.println("0");
        // clear the string:
        inputString = "";
        stringComplete = false;  
        break;
    }
  }
   
   else {
      //Read Temp, Flow, CapSense, average them
      FlowAvg = 0;
      TempAvg = 0;
      AmbientAvg = 0;
      vRef3p3Avg = 0;
      
      for (int i = 0; i < 10; i++) {
        FlowAvg += analogRead(FlowReadPin);
        TempAvg += analogRead(TempPin);
        AmbientAvg += analogRead(AmbientPin);
        vRef3p3Avg += analogRead(vRef3p3Pin);
        }
      Flow = FlowAvg / 10;
      Temp = TempAvg / 10;
      Ambient = AmbientAvg / 10;
      vRef3p3 = vRef3p3Avg / 10;
      CalTempf = Temp - 178.3788;  //Converting analog read into Farenheight (subtracted 1 Feb4th)
      CalTempf = CalTempf / 4.4138;  // y=4.4138x+179.3788
      
               //trying cap sense here:
         //check capacitive level sensor
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);

      capVal11 = levelSensor11.capacitiveSensorRaw(100);
      capVal11 = capVal11 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal10 = levelSensor10.capacitiveSensorRaw(100);
      capVal10 = capVal10 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal9 = levelSensor9.capacitiveSensorRaw(100);
      capVal9 = capVal9 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal8 = levelSensor8.capacitiveSensorRaw(100);
      capVal8 = capVal8 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal7 = levelSensor7.capacitiveSensorRaw(100);
      capVal7 = capVal7 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal6 = levelSensor6.capacitiveSensorRaw(100);
      capVal6 = capVal6 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal5 = levelSensor5.capacitiveSensorRaw(10);
      capVal5 = capVal5 / 10;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal4 = levelSensor4.capacitiveSensorRaw(100);
      capVal4 = capVal4 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal3 = levelSensor3.capacitiveSensorRaw(100);
      capVal3 = capVal3 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal2 = levelSensor2.capacitiveSensorRaw(100);
      capVal2 = capVal2 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
      
      capVal1 = levelSensor1.capacitiveSensorRaw(100);
      capVal1 = capVal1 / 100;
      delay(2);
      digitalWrite(capsensefillPin1, LOW);
      digitalWrite(capsensereadPin1, LOW);
      digitalWrite(capsensereadPin2, LOW);
      digitalWrite(capsensereadPin6, LOW);
      digitalWrite(capsensereadPin7, LOW);
      digitalWrite(capsensereadPin8, LOW);
      digitalWrite(capsensereadPin9, LOW);
      digitalWrite(capsensereadPin10, LOW);
      digitalWrite(capsensereadPin5, LOW);
      digitalWrite(capsensereadPin4, LOW);
      digitalWrite(capsensereadPin11, LOW);
      digitalWrite(capsensereadPin3, LOW);
   }  

}

    //Deals with incoming Serial Messages
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();   // get the new byte:
    inputString += inChar;    // add it to the inputString:
    if (inChar == '!') {     // if the incoming character is a newline, set a flag
      for (int i = 0; i<= 7; i++) {  //Read the Checksum
        char CheckSumRead = inputString.charAt(i);
        CheckSumIn += CheckSumRead;
      }
      if (CheckSumIn == CheckSum) {   //If the CheckSums (IDs) Match
        if (SendData) {
          Pause = true;  //if it's sending continuous data, stop for a bit
        }
        stringComplete = true;    //Tell the main program there is input
        CheckSumIn = "";
      } 
      else if ((inputString.charAt(0) == '0') && (inputString.length() == 2)) {
        Serial.println(CheckSum);
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
      else {
        Serial.println("-1");
        CheckSumIn = "";
        stringComplete = false;
        inputString = "";
      }
    }
  }
}

    //Processes incoming string
int calcfunction(String inString) {
  String actuatorValString = "";
  int StringLength = inString.length();
  int x1 = inputString.charAt(9) - '0'; //converting char 9 to int
  
  if ((x1 >= 0) && (x1 <=9)) {  //If it is a 2 digit request
    for (int i = 11; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  else {   //If it is a 1 digit request
    for (int i = 10; i <= StringLength; i++) {
      char actuatorRead = inString.charAt(i);
      actuatorValString += actuatorRead;
    }
  }
  actuatorVal = actuatorValString.toInt();
  
//  Serial.print("Actuator Val in Subprogram ");
//  Serial.println(actuatorVal);
  return actuatorVal;
}
